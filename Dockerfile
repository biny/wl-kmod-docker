FROM fedora:22
MAINTAINER Ilpo Nyyssönen, iny@iki.fi

RUN \
	dnf -y install --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm && \
	dnf -y install \
		buildsys-build-rpmfusion \
		gcc \
		kernel-devel \
		kmodtool \
		make \
		rpm-build \
		which \
	&& \
	rpm -ivh http://download1.rpmfusion.org/nonfree/fedora/releases/22/Everything/source/SRPMS/wl-kmod-6.30.223.248-8.fc22.1.src.rpm && \
	sed -i '/global buildforkernels newest/d' /root/rpmbuild/SPECS/wl-kmod.spec && \
	rpmbuild -ba --define 'buildforkernels akmod' /root/rpmbuild/SPECS/wl-kmod.spec && \
	rpmbuild -ba --define 'buildforkernels newest' --define "kernels $(rpm -q --queryformat '%{VERSION}-%{RELEASE}.%{ARCH}' kernel-devel)" /root/rpmbuild/SPECS/wl-kmod.spec && \
	rm -rf /root/rpmbuild/{BUILD,SOURCES}/* && \
	ls -lRh /root/rpmbuild
